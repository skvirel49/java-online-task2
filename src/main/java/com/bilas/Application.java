package com.bilas;

/*
Rumors. Alice is throwing a party with N other guests, including Bob. Bob starts a
rumor about Alice by telling it to one of the other guests. A person hearing this
rumor for the first time will immediately tell it to one other guest, chosen at
random from all the people at the party except Alice and the person from whom
they heard it. If a person (including Bob) hears the rumor for a second time, he or
she will not propagate it further. Write a program to estimate the probability that
everyone at the party (except Alice) will hear the rumor before it stops
propagating. Also calculate an estimate of the expected number of people to hear
the rumor.
 */

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("add number of guests:");
        int guestsNum = scanner.nextInt();

        System.out.println("add nomber of tries:");
        int triesNum = scanner.nextInt();

        int result = estimate(guestsNum, triesNum);

        System.out.println("estimate the probability that\n" +
                "everyone at the party (except Alice)\n will hear the rumor before it stops is " + result + "%");

    }

    public static boolean countRumors(int n){

//        add array og guests;
        int[] guests = new int[n];

//        guest Bob
        guests[0] = 1;

//        counter of rumors;
        int counter = 1;

//        tell rumor to random guest
        while (true){
            int r = (int) (Math.random() * n);

            guests[r] = guests[r] + 1;

            counter++;

            if (guests[r] == 2){
                break;
            }
        }
        if (counter == n){
            return true;
        } else {
            return false;
        }
    }

    public static int estimate(int x, int y){

//        add positive counter
        int countTrue = 0;

//        count positive tries
        for (int i = 0; i < y; i++) {
            if (countRumors(x)){
                countTrue++;
            }
        }

//        count astimate
        double res = (double) countTrue / y;
        int estimate =(int) (res  * 100);
        return estimate;

    }
}
